<?php
header('Content-Type: text/html; charset=UTF-8');

$Fact_NoFact = "";
$SumaImportes = 0;

$ArchXML_CFDI_CartaPorte_Traslado = "CFDI40_CartaPorte_Ingreso.xml";

$xml = file_get_contents($ArchXML_CFDI_CartaPorte_Traslado);

#== 2. Obteniendo datos del archivo .XML =========================================

$DOM = new DOMDocument('1.0', 'utf-8');
$DOM->preserveWhiteSpace = FALSE;
$DOM->loadXML($xml);

$params = $DOM->getElementsByTagName('TimbreFiscalDigital');
foreach ($params as $param) {
       $UUID     = $param->getAttribute('UUID');
       $noCertificadoSAT = $param->getAttribute('NoCertificadoSAT');
       $selloCFD = $param->getAttribute('SelloCFD');
       $selloSAT = $param->getAttribute('SelloSAT');
}

$params = $DOM->getElementsByTagName('Emisor');
$i=0;
foreach ($params as $param) {
    if ($i==0){
       $Emisor_Nom = $param->getAttribute('Nombre');
       $Emisor_RFC = $param->getAttribute('Rfc');
       $Emisor_Regimen = $param->getAttribute('RegimenFiscal');
    }
    $i++;
}

$params = $DOM->getElementsByTagName('Receptor');
$i=0;
foreach ($params as $param) {
    if ($i==0){
       $Receptor_Nom = $param->getAttribute('Nombre');
       $Receptor_RFC = $param->getAttribute('Rfc');
       $Receptor_UsoCFDI = $param->getAttribute('UsoCFDI');
    }
    $i++;
}

$params = $DOM->getElementsByTagName('Comprobante');
foreach ($params as $param) {
       $Fact_Fecha    = $param->getAttribute('Fecha');
       $Fact_Serie    = $param->getAttribute('Serie');
       $Fact_Folio    = $param->getAttribute('Folio');
       $Fact_NoCFDI   = $Fact_Serie.$Fact_Folio;
       $version       = $param->getAttribute('Version');
       $noCertificado = $param->getAttribute('NoCertificado');
       $LugarExpedicion = $param->getAttribute('LugarExpedicion');
       $subTotal      = $param->getAttribute('SubTotal');
       $total         = $param->getAttribute('Total');
       $formaDePago   = $param->getAttribute('FormaPago');
       $metodoDePago  = $param->getAttribute('MetodoPago');
       $moneda        = $param->getAttribute('Moneda');
}


if (strlen($Fact_NoFact)==0){
    $Fact_NoFact = "S/N";
}

$i=0; $ImpTot = 0;
$params = $DOM->getElementsByTagName('Concepto');
foreach ($params as $param) {
    $ArrayClaveProdServ[$i] = $param->getAttribute('ClaveProdServ');
    $ArrayClaveUnidad[$i]   = $param->getAttribute('ClaveUnidad');
    $ArrayUnidad[$i]        = $param->getAttribute('Unidad');
    $ArrayCant[$i]          = $param->getAttribute('Cantidad');
    $ArrayUniMed[$i]        = $param->getAttribute('Unidad');
    $ArrayArtSer[$i]        = $param->getAttribute('Descripcion');
    $ArrayPreUni[$i]        = $param->getAttribute('ValorUnitario');
    $ArrayImporte[$i]       = $param->getAttribute('Importe');
    $SumaImportes = $SumaImportes + $ArrayImporte[$i];
    $i++;
}


// Obteniendo datos del complemento Carta Porte =============================

$params = $DOM->getElementsByTagName('CartaPorte');
foreach ($params as $param) {
    $TotalDistRec   = $param->getAttribute('TotalDistRec');
    $Version        = $param->getAttribute('Version');
    $TranspInternac = $param->getAttribute('TranspInternac');
}

$ind = 0;
$params = $DOM->getElementsByTagName('Ubicacion');
foreach ($params as $param) {
   
    if ($ind==0){
        $Ori_TipoUbicacion   = $param->getAttribute('TipoUbicacion');
        $Ori_RFCRemitenteDestinatario   = $param->getAttribute('RFCRemitenteDestinatario');
        $Ori_FechaHoraSalidaLlegada   = $param->getAttribute('FechaHoraSalidaLlegada');
    }

    if ($ind==1){
        $Dest_TipoUbicacion   = $param->getAttribute('TipoUbicacion');
        $Dest_RFCRemitenteDestinatario   = $param->getAttribute('RFCRemitenteDestinatario');
        $Dest_FechaHoraSalidaLlegada   = $param->getAttribute('FechaHoraSalidaLlegada');
        $Dest_DistanciaRecorrida   = $param->getAttribute('DistanciaRecorrida');
    }

    $ind++;
}

$ind = 0;
$params = $DOM->getElementsByTagName('Domicilio');
foreach ($params as $param) {
   
    if ($ind==0){
        $Ori_Calle   = $param->getAttribute('Calle');
        $Ori_NumeroExterior   = $param->getAttribute('NumeroExterior');
        $Ori_Colonia   = $param->getAttribute('Colonia');
        $Ori_Municipio   = $param->getAttribute('Municipio');
        $Ori_Estado   = $param->getAttribute('Estado');
        $Ori_Pais   = $param->getAttribute('Pais');
        $Ori_CodigoPostal   = $param->getAttribute('CodigoPostal');
    }

    if ($ind==1){
        $Dest_Calle   = $param->getAttribute('Calle');
        $Dest_NumeroExterior   = $param->getAttribute('NumeroExterior');
        $Dest_Colonia   = $param->getAttribute('Colonia');
        $Dest_Localidad   = $param->getAttribute('Localidad');
        $Dest_Municipio   = $param->getAttribute('Municipio');
        $Dest_Estado   = $param->getAttribute('Estado');
        $Dest_Pais   = $param->getAttribute('Pais');
        $Dest_CodigoPostal   = $param->getAttribute('CodigoPostal');
    }
    
    $ind++;
}

$params = $DOM->getElementsByTagName('Mercancias');
foreach ($params as $param) {
    $PesoBrutoTotal = $param->getAttribute('PesoBrutoTotal');
    $UnidadPeso = $param->getAttribute('UnidadPeso');
    $NumTotalMercancias = $param->getAttribute('NumTotalMercancias');
}
    
$params = $DOM->getElementsByTagName('Mercancia');
foreach ($params as $param) {
    $BienesTransp = $param->getAttribute('BienesTransp');
    $Descripcion = $param->getAttribute('Descripcion');
    $Cantidad = $param->getAttribute('Cantidad');
    $ClaveUnidad = $param->getAttribute('ClaveUnidad');
    $PesoEnKg = $param->getAttribute('PesoEnKg');
    $ValorMercancia = $param->getAttribute('ValorMercancia');
    $Moneda = $param->getAttribute('Moneda');
}

$params = $DOM->getElementsByTagName('Autotransporte');
foreach ($params as $param) {
    $PermSCT = $param->getAttribute('PermSCT');
    $NumPermisoSCT = $param->getAttribute('NumPermisoSCT');
}

$params = $DOM->getElementsByTagName('IdentificacionVehicular');
foreach ($params as $param) {
    $ConfigVehicular = $param->getAttribute('ConfigVehicular');
    $PlacaVM = $param->getAttribute('PlacaVM');
    $AnioModeloVM = $param->getAttribute('AnioModeloVM');
}

$params = $DOM->getElementsByTagName('Seguros');
foreach ($params as $param) {
    $AseguraRespCivil = $param->getAttribute('AseguraRespCivil');
    $PolizaRespCivil = $param->getAttribute('PolizaRespCivil');
}

$params = $DOM->getElementsByTagName('Remolque');
foreach ($params as $param) {
    $SubTipoRem = $param->getAttribute('SubTipoRem');
    $Placa = $param->getAttribute('Placa');
}

$params = $DOM->getElementsByTagName('TiposFigura');
foreach ($params as $param) {
    $TipoFigura = $param->getAttribute('TipoFigura');
    $RFCFigura = $param->getAttribute('RFCFigura');
    $NumLicencia = $param->getAttribute('NumLicencia');
}


//#######################################################################################################################################################
echo '<div style="font-size: 12pt; color: #000000; margin-bottom: 10px; margin-top: 8px; ; font-family: Verdana, Arial, Helvetica, sans-serif;">';
echo 'DATOS OBTENIDOS DE UN CFDI 4.0, CARTA PORTE DE INGRESO.';
echo '</div>';   

echo '<div style="font-size: 12pt; color: #9c1891; margin-bottom: 10px; margin-top: 8px; ; font-family: Verdana, Arial, Helvetica, sans-serif;">';
echo 'DATOS DEL COMPROBANTE:';
echo '</div>';

echo '<blockquote>';

echo '<div style="margin-bottom: 5px;">UUID: <span style="color: #0540ac">' . $UUID . '</span></div>';

echo '<div style="margin-bottom: 5px;">No. de Certificado del SAT:  <span style="color: #0540ac">' . $noCertificadoSAT . '</span></div>';

echo '<div style="margin-bottom: 5px;">Sello CFD:  <span style="color: #0540ac">' . $selloCFD . '</span></div>';

echo '<div style="margin-bottom: 5px;">Sello SAT:  <span style="color: #0540ac">' . $selloSAT . '</span></div>';

echo '<div style="margin-bottom: 5px;">Emisor - Razón Social: <span style="color: #0540ac">' . $Emisor_Nom . '</span></div>';

echo '<div style="margin-bottom: 5px;">Emisor - RFC: <span style="color: #0540ac">' . $Emisor_RFC . '</span></div>';

echo '<div style="margin-bottom: 5px;">Emisor - Régimen: <span style="color: #0540ac">' . $Emisor_Regimen . '</span></div>';

echo '<div style="margin-bottom: 5px;">Receptor - Razón Social: <span style="color: #0540ac">' . $Receptor_Nom . '</span></div>';

echo '<div style="margin-bottom: 5px;">Receptor - RFC: <span style="color: #0540ac">' . $Receptor_RFC . '</span></div>';

echo '<div style="margin-bottom: 5px;">Uso CFDI: <span style="color: #0540ac">' . $Receptor_UsoCFDI . '</span></div>';

echo '<div style="margin-bottom: 5px;">CFDI - Fecha: <span style="color: #0540ac">' . $Fact_Fecha . '</span></div>';

echo '<div style="margin-bottom: 5px;">CFDI - Serie: <span style="color: #0540ac">' . $Fact_Serie . '</span></div>';

echo '<div style="margin-bottom: 5px;">CFDI - Folio: <span style="color: #0540ac">' . $Fact_Folio . '</span></div>';

echo '<div style="margin-bottom: 5px;">CFDI - Versión: <span style="color: #0540ac">' . $version . '</span></div>';

echo '<div style="margin-bottom: 5px;">CFDI - No. de certificado: <span style="color: #0540ac">' . $noCertificado . '</span></div>';

echo '<div style="margin-bottom: 5px;">CFDI - Lugar de expedición: <span style="color: #0540ac">' . $LugarExpedicion . '</span></div>';

echo '<div style="margin-bottom: 5px;">CFDI - Sub total: <span style="color: #0540ac">' . number_format($subTotal,2,'.',',') . '</span></div>';

echo '<div style="margin-bottom: 5px;">CFDI - Total: <span style="color: #0540ac">' . number_format($total,2,'.',',') . '</span></div>';

echo '<div style="margin-bottom: 5px;">CFDI - Forma de pago: <span style="color: #0540ac">' . $formaDePago . '</span></div>';

echo '<div style="margin-bottom: 5px;">CFDI - Método de pago: <span style="color: #0540ac">' . $metodoDePago . '</span></div>';

echo '<div style="margin-bottom: 5px;">CFDI - Moneda: <span style="color: #0540ac">' . $moneda . '</span></div>';


$TotRegs = count($ArrayCant);
    
for ($i=0; $i<$TotRegs; $i++){

    echo '<div style="margin-bottom: 5px;">Clave producto: <span style="color: #0540ac">' . $ArrayClaveProdServ[$i] . '</span></div>';
    echo '<div style="margin-bottom: 5px;">Clave unidad: <span style="color: #0540ac">' . $ArrayClaveUnidad[$i] . '</span></div>';
    echo '<div style="margin-bottom: 5px;">Cantidad: <span style="color: #0540ac">' . $ArrayCant[$i] . '</span></div>';
    echo '<div style="margin-bottom: 5px;">Unidad de medida: <span style="color: #0540ac">' . $ArrayUnidad[$i] . '</span></div>';
    echo '<div style="margin-bottom: 5px;">Artículo o servicio: <span style="color: #0540ac">' . $ArrayArtSer[$i] . '</span></div>';
    echo '<div style="margin-bottom: 5px;">Precio unitario: <span style="color: #0540ac">' . number_format($ArrayPreUni[$i],2) . '</span></div>';
    echo '<div style="margin-bottom: 5px;">Importe: <span style="color: #0540ac">' . number_format($ArrayImporte[$i],2) . '</span></div>';
}    

echo '</blockquote>';

echo '<div style="font-size: 12pt; color: #9c1891; margin-bottom: 10px; margin-top: 8px; ; font-family: Verdana, Arial, Helvetica, sans-serif;">';
echo 'DATOS DEL COMPLEMENTO CARTA PORTE DE TRASLADO:';
echo '</div>';   

echo '<blockquote>';

echo '<div style="margin-bottom: 5px;">Versión:  <span style="color: #0540ac">' . $Version . '</span></div>';

echo '<div style="margin-bottom: 5px;">Transporte internacional: <span style="color: #0540ac">' . $TranspInternac . '</span></div>';

echo '<div style="margin-bottom: 5px;">Total de distancia recorrida: <span style="color: #0540ac">' . $TotalDistRec . '</span></div>';

echo '<div style="margin-bottom: 5px;">Origen - Tipo ubicación: <span style="color: #0540ac">' . $Ori_TipoUbicacion . '</span></div>';

echo '<div style="margin-bottom: 5px;">Origen - RFC remitente destinatario: <span style="color: #0540ac">' . $Ori_RFCRemitenteDestinatario . '</span></div>';

echo '<div style="margin-bottom: 5px;">Origen - Fecha hora salida llegada: <span style="color: #0540ac">' . $Ori_FechaHoraSalidaLlegada . '</span></div>';

echo '<div style="margin-bottom: 5px;">Origen - Calle: <span style="color: #0540ac">' . $Ori_Calle . '</span></div>';

echo '<div style="margin-bottom: 5px;">Origen - Número exterior: <span style="color: #0540ac">' . $Ori_NumeroExterior . '</span></div>';

echo '<div style="margin-bottom: 5px;">Origen - Colonia: <span style="color: #0540ac">' . $Ori_Colonia . '</span></div>';

echo '<div style="margin-bottom: 5px;">Origen - Municipio: <span style="color: #0540ac">' . $Ori_Municipio . '</span></div>';

echo '<div style="margin-bottom: 5px;">Origen - Estado: <span style="color: #0540ac">' . $Ori_Estado . '</span></div>';

echo '<div style="margin-bottom: 5px;">Origen - País: <span style="color: #0540ac">' . $Ori_Pais . '</span></div>';

echo '<div style="margin-bottom: 5px;">Origen - Código postal: <span style="color: #0540ac">' . $Ori_CodigoPostal . '</span></div>';

echo '<div style="margin-bottom: 5px;">Destino - Tipo ubicación: <span style="color: #0540ac">' . $Dest_TipoUbicacion . '</span></div>';

echo '<div style="margin-bottom: 5px;">Destino - RFC remitente destinatario: <span style="color: #0540ac">' . $Dest_RFCRemitenteDestinatario . '</span></div>';

echo '<div style="margin-bottom: 5px;">Destino - Fecha hora salida llegada: <span style="color: #0540ac">' . $Dest_FechaHoraSalidaLlegada . '</span></div>';

echo '<div style="margin-bottom: 5px;">Destino - Distancia recorrida: <span style="color: #0540ac">' . $Dest_DistanciaRecorrida . '</span></div>';

echo '<div style="margin-bottom: 5px;">Destino - Calle: <span style="color: #0540ac">' . $Dest_Calle . '</span></div>';

echo '<div style="margin-bottom: 5px;">Destino - Número exterior: <span style="color: #0540ac">' . $Dest_NumeroExterior . '</span></div>';

echo '<div style="margin-bottom: 5px;">Destino - Colonia: <span style="color: #0540ac">' . $Dest_Colonia . '</span></div>';

echo '<div style="margin-bottom: 5px;">Destino - Localidad: <span style="color: #0540ac">' . $Dest_Localidad . '</span></div>';

echo '<div style="margin-bottom: 5px;">Destino - Municipio: <span style="color: #0540ac">' . $Dest_Municipio . '</span></div>';

echo '<div style="margin-bottom: 5px;">Destino - Estado: <span style="color: #0540ac">' . $Dest_Estado . '</span></div>';

echo '<div style="margin-bottom: 5px;">Destino - País: <span style="color: #0540ac">' . $Dest_Pais . '</span></div>';

echo '<div style="margin-bottom: 5px;">Destino - Código postal: <span style="color: #0540ac">' . $Dest_CodigoPostal . '</span></div>';

echo '<div style="margin-bottom: 5px;">Peso bruto total: <span style="color: #0540ac">' . $PesoBrutoTotal . '</span></div>';

echo '<div style="margin-bottom: 5px;">Unidad peso: <span style="color: #0540ac">' . $UnidadPeso . '</span></div>';

echo '<div style="margin-bottom: 5px;">Número total de mercancías: <span style="color: #0540ac">' . $NumTotalMercancias . '</span></div>';

echo '<div style="margin-bottom: 5px;">Clave del producto de los bienes: <span style="color: #0540ac">' . $BienesTransp . '</span></div>';

echo '<div style="margin-bottom: 5px;">Características de los bienes: <span style="color: #0540ac">' . $Descripcion . '</span></div>';

echo '<div style="margin-bottom: 5px;">Número de bienes: <span style="color: #0540ac">' . $Cantidad . '</span></div>';

echo '<div style="margin-bottom: 5px;">Clave unidad de medida: <span style="color: #0540ac">' . $ClaveUnidad . '</span></div>';

echo '<div style="margin-bottom: 5px;">Kilogramos el peso estimado de los bienes: <span style="color: #0540ac">' . $PesoEnKg . '</span></div>';

echo '<div style="margin-bottom: 5px;">Monto del valor de los bienes y/o mercancías: <span style="color: #0540ac">' . $ValorMercancia . '</span></div>';

echo '<div style="margin-bottom: 5px;">Moneda utilizada para expresar el valor de los bienes: <span style="color: #0540ac">' . $Moneda . '</span></div>';

echo '<div style="margin-bottom: 5px;">Clave del tipo de permiso proporcionado por la SCT: <span style="color: #0540ac">' . $PermSCT . '</span></div>';

echo '<div style="margin-bottom: 5px;">Número del permiso otorgado por la SCT: <span style="color: #0540ac">' . $NumPermisoSCT . '</span></div>';

echo '<div style="margin-bottom: 5px;">Clave de nomenclatura del autotransporte: <span style="color: #0540ac">' . $ConfigVehicular . '</span></div>';

echo '<div style="margin-bottom: 5px;">Solo los caracteres alfanuméricos, sin guiones ni espacios de la placa vehicular: <span style="color: #0540ac">' . $PlacaVM . '</span></div>';

echo '<div style="margin-bottom: 5px;">Año del autotransporte que es utilizado para transportar los bienes: <span style="color: #0540ac">' . $AnioModeloVM . '</span></div>';

echo '<div style="margin-bottom: 5px;">Nombre de la aseguradora que cubre los riesgos por responsabilidad civil del autotransporte utilizado: <span style="color: #0540ac">' . $AseguraRespCivil . '</span></div>';

echo '<div style="margin-bottom: 5px;">Número de póliza asignado por la aseguradora: <span style="color: #0540ac">' . $PolizaRespCivil . '</span></div>';

echo '<div style="margin-bottom: 5px;">Clave del subtipo de remolque o semirremolques: <span style="color: #0540ac">' . $SubTipoRem . '</span></div>';

echo '<div style="margin-bottom: 5px;">Solo los caracteres alfanuméricos, sin guiones ni espacios de la placa vehicular del autotransporte: <span style="color: #0540ac">' . $Placa . '</span></div>';

echo '<div style="margin-bottom: 5px;">Clave de la figura de transporte que interviene en el traslado de los bienes y/o mercancías: <span style="color: #0540ac">' . $TipoFigura . '</span></div>';

echo '<div style="margin-bottom: 5px;">RFC de la figura de transporte que interviene en el traslado de los bienes y/o mercancías: <span style="color: #0540ac">' . $RFCFigura . '</span></div>';

echo '<div style="margin-bottom: 5px;">Número de la licencia o el permiso otorgado al operador del autotransporte de carga: <span style="color: #0540ac">' . $NumLicencia . '</span></div>';

echo '</blockquote>';

echo '<div style="margin-bottom: 500px;"></div>';









