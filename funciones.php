<?php
include("cNumeroaLetra.php");

function Folio5($Num){

      if ($Num < 10){
            $Folio = "0000".$Num;
      }
      if ($Num > 9 and $Num < 100){
            $Folio = "000".$Num;
      }
      if ($Num > 99 and $Num < 1000){
            $Folio = "00".$Num;
      }
      if ($Num > 999 and $Num < 10000){
            $Folio = "0".$Num;
      }
      if ($Num > 9999 and $Num < 100000){
            $Folio = $Num;
      }
      if ($Num > 99999){
            $Folio = "N/A";
      }
      return $Folio;
}    

function NumLet($numero){
    
  $numalet= new CNumeroaletra;
  $numalet->setNumero($numero);
  //$numalet->setMoneda("Dolares");
  $numalet->setPrefijo("(");
  $numalet->setSufijo("M.N.)");
  $numalet->setGenero(1);
  $numalet->setMayusculas(1);
  $varLetra =$numalet->letra();

  return $varLetra;
}





