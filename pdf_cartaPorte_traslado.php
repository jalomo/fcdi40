<?php
header('Content-Type: text/html; charset=UTF-8');
include("fpdf/fpdf.php");
include("qrlib/qrlib.php");
include ("funciones.php");

class PDF extends FPDF
{
    function Header()
    {
        
    }

    function Footer()
    {
        $this->SetTextColor(0,0,0);
        $this->SetFont('arial','',12);
        $this->SetXY(19.4,26.2);
        $this->Cell(0.8, 0.25, $this->PageNo().'/{nb}', 0, 1,'L', 0);
    }
}

$SendaArchsCFDI = "archs_cfdi/";
$SendaArchsGraf = "archs_graf/";

$NomArchXML = $_GET["NomArchXML"];
$NomArchPDF = $_GET["NomArchPDF"];

$FechaHoraEmision = date("Y-m-d")."T".date("H:i:s"); // Esta fecha se asigna en este punto de manera PROVISIONAL para efectos de demostración 
$Obs = "";
$StatusCFDI = "ACTIVO"; // "ACTIVO" o "CANCELADO".

$Fact_NoFact = "";
$SumaImportes = 0;
$TotImpuestos = 0;

$xml = file_get_contents($SendaArchsCFDI.$NomArchXML);

#== 2. Obteniendo datos del archivo .XML =========================================
$DOM = new DOMDocument('1.0', 'utf-8');
$DOM->preserveWhiteSpace = FALSE;
$DOM->loadXML($xml);

$params = $DOM->getElementsByTagName('TimbreFiscalDigital');
foreach ($params as $param) {
       $UUID     = $param->getAttribute('UUID');
       $noCertificadoSAT = $param->getAttribute('NoCertificadoSAT');
       $selloCFD = $param->getAttribute('SelloCFD');
       $selloSAT = $param->getAttribute('SelloSAT');
}

$params = $DOM->getElementsByTagName('Emisor');
$i=0;
foreach ($params as $param) {
    if ($i==0){
       $Emisor_Nom = $param->getAttribute('Nombre');
       $Emisor_RFC = $param->getAttribute('Rfc');
       $Emisor_Regimen = $param->getAttribute('RegimenFiscal');
    }
    $i++;
}

$params = $DOM->getElementsByTagName('Receptor');
$i=0;
foreach ($params as $param) {
    if ($i==0){
       $Receptor_Nom = $param->getAttribute('Nombre');
       $Receptor_RFC = $param->getAttribute('Rfc');
       $Receptor_UsoCFDI = $param->getAttribute('UsoCFDI');
    }
    $i++;
}

$params = $DOM->getElementsByTagName('Comprobante');
foreach ($params as $param) {
       $Fact_Fecha    = $param->getAttribute('Fecha');
       $Fact_Serie    = $param->getAttribute('Serie');
       $Fact_Folio    = $param->getAttribute('Folio');
       $Fact_NoCFDI   = $Fact_Serie.$Fact_Folio;
       $version       = $param->getAttribute('Version');
       $noCertificado = $param->getAttribute('NoCertificado');
       $LugarExpedicion = $param->getAttribute('LugarExpedicion');
}

if (strlen($Fact_NoFact)==0){
    $Fact_NoFact = "S/N";
}

$i=0; $ImpTot = 0;
$params = $DOM->getElementsByTagName('Concepto');
foreach ($params as $param) {
    $ArrayClaveProdServ[$i] = $param->getAttribute('ClaveProdServ');
    $ArrayClaveUnidad[$i]   = $param->getAttribute('ClaveUnidad');
    $ArrayUnidad[$i]        = $param->getAttribute('Unidad');
    $ArrayCant[$i]          = $param->getAttribute('Cantidad');
    $ArrayUniMed[$i]        = $param->getAttribute('Unidad');
    $ArrayArtSer[$i]        = $param->getAttribute('Descripcion');
    $ArrayPreUni[$i]        = $param->getAttribute('ValorUnitario');
    $ArrayImporte[$i]       = $param->getAttribute('Importe');
    $SumaImportes = $SumaImportes + $ArrayImporte[$i];
    $i++;
}


// Obteniendo datos del complemento Carta Porte =============================

$params = $DOM->getElementsByTagName('CartaPorte');
foreach ($params as $param) {
    $TotalDistRec   = $param->getAttribute('TotalDistRec');
    $Version        = $param->getAttribute('Version');
    $TranspInternac = $param->getAttribute('TranspInternac');
}

$ind = 0;
$params = $DOM->getElementsByTagName('Ubicacion');
foreach ($params as $param) {
   
    if ($ind==0){
        $Ori_TipoUbicacion   = $param->getAttribute('TipoUbicacion');
        $Ori_RFCRemitenteDestinatario   = $param->getAttribute('RFCRemitenteDestinatario');
        $Ori_FechaHoraSalidaLlegada   = $param->getAttribute('FechaHoraSalidaLlegada');
    }

    if ($ind==1){
        $Dest_TipoUbicacion   = $param->getAttribute('TipoUbicacion');
        $Dest_RFCRemitenteDestinatario   = $param->getAttribute('RFCRemitenteDestinatario');
        $Dest_FechaHoraSalidaLlegada   = $param->getAttribute('FechaHoraSalidaLlegada');
        $Dest_DistanciaRecorrida   = $param->getAttribute('DistanciaRecorrida');
    }

    $ind++;
}

$ind = 0;
$params = $DOM->getElementsByTagName('Domicilio');
foreach ($params as $param) {
   
    if ($ind==0){
        $Ori_Calle   = $param->getAttribute('Calle');
        $Ori_NumeroExterior   = $param->getAttribute('NumeroExterior');
        $Ori_Colonia   = $param->getAttribute('Colonia');
        $Ori_Municipio   = $param->getAttribute('Municipio');
        $Ori_Estado   = $param->getAttribute('Estado');
        $Ori_Pais   = $param->getAttribute('Pais');
        $Ori_CodigoPostal   = $param->getAttribute('CodigoPostal');
    }

    if ($ind==1){
        $Dest_Calle   = $param->getAttribute('Calle');
        $Dest_NumeroExterior   = $param->getAttribute('NumeroExterior');
        $Dest_Colonia   = $param->getAttribute('Colonia');
        $Dest_Localidad   = $param->getAttribute('Localidad');
        $Dest_Municipio   = $param->getAttribute('Municipio');
        $Dest_Estado   = $param->getAttribute('Estado');
        $Dest_Pais   = $param->getAttribute('Pais');
        $Dest_CodigoPostal   = $param->getAttribute('CodigoPostal');
    }
    
    $ind++;
}

$params = $DOM->getElementsByTagName('Mercancias');
foreach ($params as $param) {
    $PesoBrutoTotal = $param->getAttribute('PesoBrutoTotal');
    $UnidadPeso = $param->getAttribute('UnidadPeso');
    $NumTotalMercancias = $param->getAttribute('NumTotalMercancias');
}
    
$params = $DOM->getElementsByTagName('Mercancia');
foreach ($params as $param) {
    $BienesTransp = $param->getAttribute('BienesTransp');
    $Descripcion = $param->getAttribute('Descripcion');
    $Cantidad = $param->getAttribute('Cantidad');
    $ClaveUnidad = $param->getAttribute('ClaveUnidad');
    $PesoEnKg = $param->getAttribute('PesoEnKg');
    $ValorMercancia = $param->getAttribute('ValorMercancia');
    $Moneda = $param->getAttribute('Moneda');
}

$params = $DOM->getElementsByTagName('Autotransporte');
foreach ($params as $param) {
    $PermSCT = $param->getAttribute('PermSCT');
    $NumPermisoSCT = $param->getAttribute('NumPermisoSCT');
}

$params = $DOM->getElementsByTagName('IdentificacionVehicular');
foreach ($params as $param) {
    $ConfigVehicular = $param->getAttribute('ConfigVehicular');
    $PlacaVM = $param->getAttribute('PlacaVM');
    $AnioModeloVM = $param->getAttribute('AnioModeloVM');
}

$params = $DOM->getElementsByTagName('Seguros');
foreach ($params as $param) {
    $AseguraRespCivil = $param->getAttribute('AseguraRespCivil');
    $PolizaRespCivil = $param->getAttribute('PolizaRespCivil');
}

$params = $DOM->getElementsByTagName('Remolque');
foreach ($params as $param) {
    $SubTipoRem = $param->getAttribute('SubTipoRem');
    $Placa = $param->getAttribute('Placa');
}

$params = $DOM->getElementsByTagName('TiposFigura');
foreach ($params as $param) {
    $TipoFigura = $param->getAttribute('TipoFigura');
    $RFCFigura = $param->getAttribute('RFCFigura');
    $NumLicencia = $param->getAttribute('NumLicencia');
}

    $DOM = new DOMDocument('1.0', 'utf-8');
    $DOM->preserveWhiteSpace = FALSE;
    $DOM->loadXML($xml);

    $params = $DOM->getElementsByTagName('TimbreFiscalDigital');
    foreach ($params as $param) {
           $UUID     = $param->getAttribute('UUID');
           $noCertificadoSAT = $param->getAttribute('NoCertificadoSAT');
           $selloCFD = $param->getAttribute('SelloCFD');
           $selloSAT = $param->getAttribute('SelloSAT');
    }      

    $params = $DOM->getElementsByTagName('Emisor');
    $i=0;
    foreach ($params as $param) {
        if ($i==0){
           $Emisor_Nom = $param->getAttribute('Nombre');
           $Emisor_RFC = $param->getAttribute('Rfc');
           $Emisor_Regimen = $param->getAttribute('RegimenFiscal');
        }
        $i++;
    }    
    
    $params = $DOM->getElementsByTagName('Receptor');
    $i=0;
    foreach ($params as $param) {
        if ($i==0){
           $Receptor_Nom = $param->getAttribute('Nombre');
           $Receptor_RFC = $param->getAttribute('Rfc');
           $Receptor_UsoCFDI = $param->getAttribute('UsoCFDI');
        }
        $i++;
    }    
    
    
    $params = $DOM->getElementsByTagName('Comprobante');
    foreach ($params as $param) {
           $Fact_Fecha    = $param->getAttribute('Fecha');
           $Fact_Serie    = $param->getAttribute('Serie');
           $Fact_Folio    = $param->getAttribute('Folio');
           $Fact_NoFact   = $Fact_Serie.$Fact_Folio;
           $descuento     = $param->getAttribute('Descuento');
           $subTotal      = $param->getAttribute('SubTotal');
           $total         = $param->getAttribute('Total');
           $version       = $param->getAttribute('Version');
           $noCertificado = $param->getAttribute('NoCertificado');
           $formaDePago   = $param->getAttribute('FormaPago');
           $metodoDePago  = $param->getAttribute('MetodoPago');
           $NumCtaPago    = "";
           $LugarExpedicion = $param->getAttribute('LugarExpedicion');
    }

    if (strlen($Fact_NoFact)==0){
        $Fact_NoFact = "S/N";
    }
    
    $i=0; $ImpTot = 0;
    $params = $DOM->getElementsByTagName('Concepto');
    foreach ($params as $param) {
           $ArrayClaveProdServ[$i] = $param->getAttribute('ClaveProdServ');
           $ArrayClaveUnidad[$i]   = $param->getAttribute('ClaveUnidad');
           $ArrayUnidad[$i]        = $param->getAttribute('Unidad');
           $ArrayCant[$i]          = $param->getAttribute('Cantidad');
           $ArrayUniMed[$i]        = $param->getAttribute('Unidad');
           $ArrayArtSer[$i]        = $param->getAttribute('Descripcion');
           $ArrayPreUni[$i]        = $param->getAttribute('ValorUnitario');
           $ArrayImporte[$i]       = $param->getAttribute('Importe');
           $SumaImportes = $SumaImportes + $ArrayImporte[$i];
           $i++;
    }       
        
    $CadOri = "||".$UUID."|".$Fact_Fecha."|".$selloCFD."|".$noCertificado."||";
    
#== 3. Crear archivo .PNG con codigo bidimensional =================================
$filename = $SendaArchsGraf."/Img_".$UUID.".png";
$CadImpTot = ProcesImpTot($total);
$Cadena = "?re=".$Emisor_RFC."&rr=".$Receptor_RFC."&tt=".$CadImpTot."&id=".$UUID;
QRcode::png($Cadena, $filename, 'H', 3, 2);    
chmod($filename, 0777);  


#== 4. Construyendo el documentos con la librería FPDF =======================================

$pdf=new FPDF('P','cm','Letter');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->AddFont('IDAutomationHC39M','','IDAutomationHC39M.php');
$pdf->AddFont('verdana','','verdana.php');
$pdf->SetAutoPageBreak(true);
$pdf->SetMargins(0, 0, 0);
$pdf->SetLineWidth(0.02);
$pdf->SetFillColor(0,0,0);

####### ENCABEZADO DE LA FACTURA #############################################################
    
    $X = 0;
    $Y = 0;

    $pdf->image("archs_graf/Membrete_Fact.jpg",$X+1, $Y+1 , 9, 2.3);
    $pdf->image("archs_graf/LogoSAT.jpg",$X+16.6, $Y+3.4 , 0, 0);
    
    $pdf->SetTextColor(7,100,30);
    $pdf->SetFont('arial','B',13);
    $pdf->SetXY($X+10.5,$Y+1.25+0.1);
    $pdf->Cell(1.5, 0.25, "CARTA PORTE DE TRASLADO.", 0, 1,'L', 0);

        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('arial','B',10);
        $pdf->SetXY($X+15.5,$Y+1.46+0.5);
        $pdf->Cell(2.5, 0.25, "FOLIO Y SERIE:", 0, 1,'R', 0);

        $pdf->SetTextColor(171,17,17);
        $pdf->SetFont('arial','',14);
        $pdf->SetXY($X+18,$Y+1.45+0.5);
        $pdf->Cell(1, 0.25, $Fact_NoFact, 0, 1,'L', 0);

        
    $pdf->SetTextColor(0,0,0);
    $pdf->SetFont('arial','B',9);
    $pdf->SetXY($X+10.5,$Y+2.05);
    $pdf->Cell(2, 0.25, "FOLIO FISCAL:", 0, 1,'L', 0);

    $pdf->SetTextColor(17,71,121);
    $pdf->SetFont('arial','',11);
    $pdf->SetXY($X+10.5+0.5,$Y+2.5);
    $pdf->Cell(2, 0.25, $UUID, 0, 1,'L', 0);
    
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('arial','B',9);
        $pdf->SetXY($X+10.5,$Y+2.5+0.6);
        $pdf->Cell(2, 0.25, "CERTIFICADO SAT:", 0, 1,'L', 0);

        $pdf->SetTextColor(17,71,121);
        $pdf->SetFont('arial','',10);
        $pdf->SetXY($X+10.5+0.5,$Y+2.5+0.6+0.4);
        $pdf->Cell(2, 0.25, $noCertificadoSAT, 0, 1,'L', 0);

    
    $pdf->SetTextColor(0,0,0);
    $pdf->SetFont('arial','B',9);
    $pdf->SetXY($X+10.5,$Y+4.06);
    $pdf->Cell(2, 0.25, "CERTIFICADO DEL EMISOR:", 0, 1,'L', 0);

    $pdf->SetTextColor(17,71,121);
    $pdf->SetFont('arial','',10);
    $pdf->SetXY($X+10.5+0.5,$Y+4.06+0.4);
    $pdf->Cell(2, 0.25, $noCertificado, 0, 1,'L', 0);

    
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('arial','B',9);
        $pdf->SetXY($X+10.5,$Y+4.46+0.5);
        $pdf->Cell(2, 0.25, utf8_decode("FECHA HORA DE EMISIÓN:"), 0, 1,'L', 0);
    
        $pdf->SetTextColor(17,71,121);
        $pdf->SetFont('arial','',9);
        $pdf->SetXY($X+10.5+0.5,$Y+4.46+0.5+0.34);
        $pdf->MultiCell(9.4, 0.35, utf8_decode($FechaHoraEmision), 0, 'L');

        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('arial','B',9);
        $pdf->SetXY($X+10.5,$Y+6);
        $pdf->Cell(2, 0.25, utf8_decode("FECHA HORA DE CERTIFICACIÓN:"), 0, 1,'L', 0);
        
        $pdf->SetTextColor(17,71,121);
        $pdf->SetFont('arial','',9);
        $pdf->SetXY($X+10.5+5.5,$Y+6);
        $pdf->Cell(2, 0.25, $Fact_Fecha, 0, 1,'L', 0);
        
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('arial','B',9);
        $pdf->SetXY($X+10.5,$Y+6.6);
        $pdf->Cell(2, 0.25, utf8_decode("RÉGIMEN FISCAL:"), 0, 1,'L', 0);

        $pdf->SetFont('arial','',9);
        $pdf->SetTextColor(17,71,121);
        $pdf->SetXY($X+13.5,$Y+6.6);
        $pdf->Cell(1, 0.25, utf8_decode($Emisor_Regimen), 0, 1,'L', 0);

    $pdf->RoundedRect($X+10.4, $Y+1, 10, 6.14, 0.2, '');

    //======================================================================
    
    $pdf->SetTextColor(7,100,30);
    $pdf->SetFont('arial','B',11);
    $pdf->SetXY($X+1.05,$Y+3.7);
    $pdf->Cell(1, 0.25, "EMISOR:", 0, 1,'L', 0);
    
        $pdf->SetTextColor(17,71,121);
        $pdf->SetFont('arial','',9);
        $pdf->SetXY($X+1.4,$Y+3.7+0.4);
        $pdf->MultiCell(8.6, 0.35, utf8_decode($Emisor_Nom), 0, 'L');
    
    $pdf->SetTextColor(0,0,0);
    $pdf->SetFont('arial','B',10);
    $pdf->SetXY($X+1.05,$Y+3.7+0.45+1.25);
    $pdf->Cell(1, 0.25, "RFC:", 0, 1,'L', 0);
    
        $pdf->SetTextColor(17,71,121);
        $pdf->SetFont('arial','',11);
        $pdf->SetXY($X+2.05,$Y+3.7+0.45+1.25);
        $pdf->Cell(1, 0.25, utf8_decode($Emisor_RFC), 0, 1,'L', 0);
    
    $pdf->SetTextColor(0,0,0);
    $pdf->SetFont('arial','B',9);
    $pdf->SetXY($X+1.05,$Y+3.7+0.45+1.8);
    $pdf->Cell(1, 0.25, utf8_decode("LUGAR DE EXPEDICIÓN (CÓDIGO POSTAL):"), 0, 1,'L', 0);
    
        $pdf->SetTextColor(17,71,121);
        $pdf->SetFont('arial','',10);
        $pdf->SetXY($X+1.05+7,$Y+3.7+0.45+1.8);
        $pdf->Cell(1, 0.25, $LugarExpedicion, 0, 1,'L', 0);
        
    $pdf->RoundedRect($X+1, $Y+3.5, 9, 3.64, 0.2, '');
    
    $Y = $Y -1;
    
    //======================================================================
    
    $pdf->SetTextColor(7,100,30);
    $pdf->SetFont('arial','B',11);
    $pdf->SetXY($X+1.05,$Y+8.6);
    $pdf->Cell(1, 0.25, "RECEPTOR:", 0, 1,'L', 0);

    $pdf->SetTextColor(17,71,121);
    $pdf->SetFont('arial','',9);
    $pdf->SetXY($X+1.5,$Y+8.4+0.65);
    $pdf->Cell(1, 0.25, utf8_decode($Receptor_Nom), 0, 1,'L', 0);
    
    $pdf->SetTextColor(0,0,0);
    $pdf->SetFont('arial','B',10);
    $pdf->SetXY($X+1.05,$Y+9.5+0.1);
    $pdf->Cell(1, 0.25, "RFC:", 0, 1,'L', 0);
    
    $pdf->SetTextColor(17,71,121);
    $pdf->SetFont('arial','',11);
    $pdf->SetXY($X+1.05+0.9,$Y+9.5+0.1);
    $pdf->Cell(1, 0.25, $Receptor_RFC, 0, 1,'L', 0);
    
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('arial','B',10);
        $pdf->SetXY($X+8,$Y+9.5+0.1);
        $pdf->Cell(2, 0.25, utf8_decode("USO CFDI:"), 0, 1,'L', 0);    

        $pdf->SetTextColor(17,71,121);
        $pdf->SetFont('arial','',11);
        $pdf->SetXY($X+8+2,$Y+9.5+0.1);
        $pdf->Cell(1, 0.25, $Receptor_UsoCFDI, 0, 1,'L', 0);
        
        
$pdf->SetTextColor(199,199,199);
$pdf->SetFont('arial','BI',29);
$pdf->SetXY($X+15.4,$Y+9.16);
$pdf->Cell(2, 0.25, utf8_decode("CFDI 3.3"), 0, 1,'L', 0);    
    
    $pdf->RoundedRect($X+1, $Y+8.4, 19.4, 1.7, 0.2, '');
    
    VerifStatusCFDI($pdf, $StatusCFDI);
    
    
#== Registros de artículos ================================================

    $pdf->SetTextColor(7,100,30);
    $pdf->SetFont('arial','B',11);
    $pdf->SetXY($X+0.8,$Y+10.5);
    $pdf->Cell(1, 0.25, "CONCEPTOS:", 0, 1,'L', 0);    
    
    $Y = $pdf->GetY()+0.9;
    
    $Regs = 0;
    $RefAgregPag = 0;

    Titulos($pdf, $Y-0.8);    
    
    $TotRegs = count($ArrayCant);
    
    for ($i=0; $i<$TotRegs; $i++){
        
            $pdf->SetFont('arial','',8);
            
            $pdf->SetTextColor(171,18,18);
            $pdf->SetXY($X+1,$Y);
            $pdf->Cell(1.8, 0.35, $ArrayClaveProdServ[$i], 0, 0,'C', 0);

            $pdf->SetXY($X+2.85,$Y);
            $pdf->Cell(1.3, 0.35, $ArrayClaveUnidad[$i], 0, 0,'C', 0);
            
            $pdf->SetTextColor(17,71,121);
            $pdf->SetXY($X+4.2,$Y);
            $pdf->Cell(1.5, 0.35, number_format($ArrayCant[$i],2,'.',''), 0, 0,'C', 0);
            
            $pdf->SetXY($X+5.75,$Y);
            $pdf->Cell(2.1, 0.35, $ArrayUnidad[$i], 0, 0,'L', 0);
            
            $pdf->SetXY($X+7.9,$Y);
            $pdf->MultiCell(8.5, 0.35, utf8_decode($ArrayArtSer[$i]), 0, 'L', 0);
            $pdf->Write(0.4,'');
            $YY = $pdf->GetY()+0.18;
            $Puntero = $pdf->GetY();
            
            $pdf->SetXY($X+16.6,$Y);
            $pdf->Cell(1.7, 0.35, number_format($ArrayPreUni[$i],2,'.',''), 0, 0,'R', 0);

            $pdf->SetXY($X+18.5,$Y);
            $pdf->Cell(1.8, 0.35, number_format($ArrayImporte[$i],2,'.',''), 0, 0,'R', 0);

            $pdf->line($X+1, $Y-0.1, $X+20.4, $Y-0.1);

            $Y = $YY;
            $Regs++;

            if ($Puntero>23.5){

                if ($TotRegs>$Regs){

                    $pdf->AddPage();
                    Titulos($pdf, 1.5);
                    VerifStatusCFDI($pdf, $StatusCFDI);
                    $Y = 2.4;
                }else{
                    $pdf->AddPage();
                    VerifStatusCFDI($pdf, $StatusCFDI);
                    $Y = 1.4;
                }
            }  
    }    
    
// Datos del Complemento de Carta Porte de Traslado =========================================================    

$Y = 11; // Reajustando coordenada Y

$pdf->line($X+1, $Y+0.7, $X+20.4, $Y+0.7);

$pdf->SetTextColor(7,100,30);
$pdf->SetFont('arial','B',10);
$pdf->SetXY($X+1,$Y+1);
$pdf->Cell(1, 0.25, "DATOS COMPLEMENTO CARTA PORTE TRASLADO:", 0, 1,'L', 0);

$pdf->SetFont('arial','',11);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+1.5);
$pdf->Cell(2, 0.25, utf8_decode("Versión:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+2.8,$Y+1.5);
$pdf->Cell(2, 0.25, $Version, 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+1.5+0.5);
$pdf->Cell(2, 0.25, utf8_decode("Transporte internacional:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+5.5,$Y+1.5+0.5);
$pdf->Cell(2, 0.25, $TranspInternac, 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+1.5+(0.5*2));
$pdf->Cell(2, 0.25, utf8_decode("Total de distancia recorrida:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+6,$Y+1.5+(0.5*2));
$pdf->Cell(2, 0.25, $TotalDistRec, 0, 1,'L', 0);

$pdf->SetTextColor(128,0,128);
$pdf->SetFont('arial','B',10);
$pdf->SetXY($X+1,14.3);
$pdf->Cell(1, 0.25, "ORIGEN", 0, 1,'L', 0);

$Y = $Y + 0.8;

$pdf->SetFont('arial','',11);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+1.5+(0.5*3));
$pdf->Cell(2, 0.25, utf8_decode("Tipo ubicación:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+5.3-1.5,$Y+1.5+(0.5*3));
$pdf->Cell(2, 0.25, $Ori_TipoUbicacion, 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+1.5+(0.5*4));
$pdf->Cell(2, 0.25, utf8_decode("RFC remitente destinatario:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+7.5-1.5,$Y+1.5+(0.5*4));
$pdf->Cell(2, 0.25, $Ori_RFCRemitenteDestinatario, 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+1.5+(0.5*5));
$pdf->Cell(2, 0.25, utf8_decode("Fecha hora salida llegada:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+7.2-1.5,$Y+1.5+(0.5*5));
$pdf->Cell(2, 0.25, $Ori_FechaHoraSalidaLlegada, 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+1.5+(0.5*6));
$pdf->Cell(2, 0.25, utf8_decode("Calle:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+3.7-1.5,$Y+1.5+(0.5*6));
$pdf->Cell(2, 0.25, $Ori_Calle, 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+1.5+(0.5*7));
$pdf->Cell(2, 0.25, utf8_decode("Número exterior:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+5.6-1.5,$Y+1.5+(0.5*7));
$pdf->Cell(2, 0.25, $Ori_NumeroExterior, 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+1.5+(0.5*8));
$pdf->Cell(2, 0.25, utf8_decode("Colonia:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+4.1-1.5,$Y+1.5+(0.5*8));
$pdf->Cell(2, 0.25, $Ori_Colonia, 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+1.5+(0.5*9));
$pdf->Cell(2, 0.25, utf8_decode("Municipio:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+4.4-1.5,$Y+1.5+(0.5*9));
$pdf->Cell(2, 0.25, $Ori_Municipio, 0, 1,'L', 0);
    
$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+1.5+(0.5*10));
$pdf->Cell(2, 0.25, utf8_decode("Estado:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+4-1.5,$Y+1.5+(0.5*10));
$pdf->Cell(2, 0.25, $Ori_Estado, 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+1.5+(0.5*11));
$pdf->Cell(2, 0.25, utf8_decode("País:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+3.8-1.5,$Y+1.5+(0.5*11));
$pdf->Cell(2, 0.25, $Ori_Pais, 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+1.5+(0.5*12));
$pdf->Cell(2, 0.25, utf8_decode("Código postal:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+5.2-1.5,$Y+1.5+(0.5*12));
$pdf->Cell(2, 0.25, $Ori_CodigoPostal, 0, 1,'L', 0);

$pdf->SetTextColor(128,0,128);
$pdf->SetFont('arial','B',10);
$pdf->SetXY($X+11.2,14.3);
$pdf->Cell(1, 0.25, "DESTINO", 0, 1,'L', 0);

$Y = 11.8;

$pdf->SetFont('arial','',11);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1+10.2,$Y+1.5+(0.5*3));
$pdf->Cell(2, 0.25, utf8_decode("Tipo ubicación:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+5.3-1.5+10.2,$Y+1.5+(0.5*3));
$pdf->Cell(2, 0.25, $Dest_TipoUbicacion, 0, 1,'L', 0);


$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1+10.2,$Y+1.5+(0.5*4));
$pdf->Cell(2, 0.25, utf8_decode("RFC remitente destinatario:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+7.5-1.5+10.2,$Y+1.5+(0.5*4));
$pdf->Cell(2, 0.25, $Dest_RFCRemitenteDestinatario, 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1+10.2,$Y+1.5+(0.5*5));
$pdf->Cell(2, 0.25, utf8_decode("Fecha hora salida llegada:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+7.2-1.5+10.2,$Y+1.5+(0.5*5));
$pdf->Cell(2, 0.25, $Dest_FechaHoraSalidaLlegada, 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1+10.2,$Y+1.5+(0.5*6));
$pdf->Cell(2, 0.25, utf8_decode("Calle:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+3.7-1.5+10.2,$Y+1.5+(0.5*6));
$pdf->Cell(2, 0.25, $Dest_Calle, 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1+10.2,$Y+1.5+(0.5*7));
$pdf->Cell(2, 0.25, utf8_decode("Número exterior:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+5.6-1.5+10.2,$Y+1.5+(0.5*7));
$pdf->Cell(2, 0.25, $Dest_NumeroExterior, 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1+10.2,$Y+1.5+(0.5*8));
$pdf->Cell(2, 0.25, utf8_decode("Colonia:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+4.1-1.5+10.2,$Y+1.5+(0.5*8));
$pdf->Cell(2, 0.25, $Dest_Colonia, 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1+10.2,$Y+1.5+(0.5*9));
$pdf->Cell(2, 0.25, utf8_decode("Localidad:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+4.1-1.2+10.2,$Y+1.5+(0.5*9));
$pdf->Cell(2, 0.25, $Dest_Localidad, 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1+10.2,$Y+1.5+(0.5*10));
$pdf->Cell(2, 0.25, utf8_decode("Municipio:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+4.4-1.5+10.2,$Y+1.5+(0.5*10));
$pdf->Cell(2, 0.25, $Dest_Municipio, 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1+10.2,$Y+1.5+(0.5*11));
$pdf->Cell(2, 0.25, utf8_decode("Estado:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+4-1.5+10.2,$Y+1.5+(0.5*11));
$pdf->Cell(2, 0.25, $Dest_Estado, 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1+10.2,$Y+1.5+(0.5*12));
$pdf->Cell(2, 0.25, utf8_decode("País:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+3.8-1.5+10.2,$Y+1.5+(0.5*12));
$pdf->Cell(2, 0.25, $Dest_Pais, 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1+10.2,$Y+1.5+(0.5*13));
$pdf->Cell(2, 0.25, utf8_decode("Código postal:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+5.2-1.5+10.2,$Y+1.5+(0.5*13));
$pdf->Cell(2, 0.25, $Dest_CodigoPostal, 0, 1,'L', 0);


$pdf->SetTextColor(128,0,128);
$pdf->SetFont('arial','B',10);
$pdf->SetXY($X+1,$Y+2.5+(0.5*13));
$pdf->Cell(1, 0.25, utf8_decode("MERCANCÍAS, BIENES, TRANSPORTE Y OTROS DATOS."), 0, 1,'L', 0);

$pdf->SetFont('arial','',11);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+2.5+(0.5*14));
$pdf->Cell(2, 0.25, utf8_decode("Peso bruto total:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+5.2-1.2,$Y+2.5+(0.5*14));
$pdf->Cell(2, 0.25, $PesoBrutoTotal, 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+2.5+(0.5*15));
$pdf->Cell(2, 0.25, utf8_decode("Unidad peso:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+3.5,$Y+2.5+(0.5*15));
$pdf->Cell(2, 0.25, $UnidadPeso, 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+2.5+(0.5*16));
$pdf->Cell(2, 0.25, utf8_decode("Número total de mercancías:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+3.5+2.7,$Y+2.5+(0.5*16));
$pdf->Cell(2, 0.25, $NumTotalMercancias, 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+2.5+(0.5*17));
$pdf->Cell(2, 0.25, utf8_decode("Clave del producto de los bienes:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+3.5+3.4,$Y+2.5+(0.5*17));
$pdf->Cell(2, 0.25, $BienesTransp, 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+2.5+(0.5*18));
$pdf->Cell(2, 0.25, utf8_decode("Características de los bienes:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+3.5+2.8,$Y+2.5+(0.5*18));
$pdf->Cell(2, 0.25, $Descripcion, 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+2.5+(0.5*19));
$pdf->Cell(2, 0.25, utf8_decode("Número de bienes:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+3.5+0.9,$Y+2.5+(0.5*19));
$pdf->Cell(2, 0.25, $Cantidad, 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+2.5+(0.5*20));
$pdf->Cell(2, 0.25, utf8_decode("Clave unidad de medida:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+3.5+2,$Y+2.5+(0.5*20));
$pdf->Cell(2, 0.25, $ClaveUnidad, 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+2.5+(0.5*21));
$pdf->Cell(2, 0.25, utf8_decode("Kilogramos el peso estimado de los bienes:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+3.5+5.2,$Y+2.5+(0.5*21));
$pdf->Cell(2, 0.25, $PesoEnKg, 0, 1,'L', 0);

$pdf->AddPage();

$Y = 1;

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+0+(0.5*1));
$pdf->Cell(2, 0.25, utf8_decode("Monto del valor de los bienes y/o mercancías:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+3.5+5.6,$Y+0+(0.5*1));
$pdf->Cell(2, 0.25, number_format($ValorMercancia,2,'.',','), 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+0+(0.5*2));
$pdf->Cell(2, 0.25, utf8_decode("Moneda utilizada para expresar el valor de los bienes:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+3.5+7,$Y+0+(0.5*2));
$pdf->Cell(2, 0.25, utf8_decode($Moneda), 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+0+(0.5*3));
$pdf->Cell(2, 0.25, utf8_decode("Clave del tipo de permiso proporcionado por la SCT:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+3.5+6.7,$Y+0+(0.5*3));
$pdf->Cell(2, 0.25, utf8_decode($PermSCT), 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+0+(0.5*4));
$pdf->Cell(2, 0.25, utf8_decode("Número del permiso otorgado por la SCT:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+3.5+4.9,$Y+0+(0.5*4));
$pdf->Cell(2, 0.25, utf8_decode($NumPermisoSCT), 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+0+(0.5*5));
$pdf->Cell(2, 0.25, utf8_decode("Clave de nomenclatura del autotransporte:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+3.5+5.1,$Y+0+(0.5*5));
$pdf->Cell(2, 0.25, utf8_decode($ConfigVehicular), 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+0+(0.5*6));
$pdf->Cell(2, 0.25, utf8_decode("Solo los caracteres alfanuméricos, sin guiones ni espacios de la placa vehicular:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+3.5+11.5,$Y+0+(0.5*6));
$pdf->Cell(2, 0.25, utf8_decode($PlacaVM), 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+0+(0.5*7));
$pdf->Cell(2, 0.25, utf8_decode("Año del autotransporte que es utilizado para transportar los bienes:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+3.5+9.2,$Y+0+(0.5*7));
$pdf->Cell(2, 0.25, utf8_decode($AnioModeloVM), 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+0+(0.5*8));
$pdf->Cell(2, 0.25, utf8_decode("Nombre de la aseguradora del autotransporte utilizado:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+3.5+7.2,$Y+0+(0.5*8));
$pdf->Cell(2, 0.25, utf8_decode($AseguraRespCivil), 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+0+(0.5*9));
$pdf->Cell(2, 0.25, utf8_decode("Número de póliza asignado por la aseguradora:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+3.5+5.9,$Y+0+(0.5*9));
$pdf->Cell(2, 0.25, utf8_decode($PolizaRespCivil), 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+0+(0.5*10));
$pdf->Cell(2, 0.25, utf8_decode("Clave del subtipo de remolque o semirremolques:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+3.5+6.2,$Y+0+(0.5*10));
$pdf->Cell(2, 0.25, utf8_decode($SubTipoRem), 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+0+(0.5*11));
$pdf->Cell(2, 0.25, utf8_decode("Clave de la figura de transporte que interviene en el traslado de los bienes y/o mercancías:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+3.5+13.3,$Y+0+(0.5*11));
$pdf->Cell(2, 0.25, utf8_decode($TipoFigura), 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+0+(0.5*12));
$pdf->Cell(2, 0.25, utf8_decode("RFC de la figura de transporte que interviene en el traslado de los bienes y/o mercancías:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+3.5+13.1,$Y+0+(0.5*12));
$pdf->Cell(2, 0.25, utf8_decode($RFCFigura), 0, 1,'L', 0);

$pdf->SetTextColor(70,70,70);
$pdf->SetXY($X+1,$Y+0+(0.5*13));
$pdf->Cell(2, 0.25, utf8_decode("Número de la licencia o el permiso otorgado al operador del autotransporte de carga:"), 0, 1,'L', 0);

$pdf->SetTextColor(17,71,121);
$pdf->SetXY($X+3.5+12.3,$Y+0+(0.5*13));
$pdf->Cell(2, 0.25, utf8_decode($NumLicencia), 0, 1,'L', 0);

// Impresión de código QR y firmas electrónicas.

$Y = $Y - 1;

$pdf->SetTextColor(0,0,0);
$pdf->SetFont('arial','B',9); 
$pdf->SetXY(1.2,22.9+$Y-0.25 -1.5);
$pdf->Cell(1.7,0.30, "Sello digital del CFDI:", 0, 1,'L', 0);    

    $pdf->SetTextColor(17,71,121);
    $pdf->SetFont('arial','',7); 
    $pdf->SetXY(1.2,+22.9+0.35+$Y-0.2 -1.5);
    $pdf->MultiCell(19.4, 0.25, $selloCFD, 0, 'L', 0);

$pdf->SetTextColor(0,0,0);
$pdf->SetFont('arial','B',9); 
$pdf->SetXY(4.2,21.9+2+$Y-1.2-0.2);
$pdf->Cell(1.7, 0.30, "Sello del SAT:", 0, 1,'L', 0);    

    $pdf->SetTextColor(17,71,121);
    $pdf->SetFont('arial','',7); 
    $pdf->SetXY(4.2,21.9+0.35+2+$Y-1.2-0.2);
    $pdf->MultiCell(16.1, 0.25, $selloSAT, 0, 'L', 0);    

$pdf->SetTextColor(0,0,0);    
$pdf->SetFont('arial','B',9); 
$pdf->SetXY(4.2,25+$Y-0.7-0.25);
$pdf->Cell(1.7, 0.30, utf8_decode("Cadena original del complemento de certificación digital del SAT:"), 0, 1,'L', 0);    

    $pdf->SetTextColor(17,71,121);
    $pdf->SetFont('arial','',7); 
    $pdf->SetXY(4.2,25.1+0.25+$Y-0.7-0.2);
    $pdf->MultiCell(16.1, 0.25, $CadOri, 0, 'L', 0);    

$pdf->SetTextColor(0,0,0);
$pdf->SetFont('arial','B',10); 
$pdf->SetXY(4.2,26.36+$Y -0.2);
$pdf->Cell(15.6, 0.30, utf8_decode("===== Este documento es una representación impresa de un CFDI ====="), 0, 1,'C', 0);    
    
$pdf->Image($filename,1.2,23.8+$Y-1.1-0.25,3,3,'PNG');



//======================================================================================================================
$pdf->Output($SendaArchsCFDI.$NomArchPDF, 'F'); // Se graba el documento .PDF en el disco duro o unidad de estado sólido.

chmod ($SendaArchsCFDI.$NomArchPDF,0777);  // <-- Descomentar si está utilizando el sistema operativo LINUX.
    
$pdf->Output($SendaArchsCFDI.$NomArchPDF, 'I'); // Se muestra el documento .PDF en el navegador.



function Titulos($pdf, $Y){
    
    $Y = $Y + 0.24;
    
    $pdf->SetTextColor(0,0,0);
    $pdf->SetFont('arial','B',9);    
    
    $pdf->SetXY(1.1,$Y);
    $pdf->Cell(1.5, 0.30, "ClaProdServ", 0, 1,'C', 0);

    $pdf->SetXY(1.1+1.65,$Y);
    $pdf->Cell(1.5, 0.30, "ClaUni", 0, 1,'C', 0);

    $pdf->SetXY(1.1+3.3,$Y);
    $pdf->Cell(1, 0.30, "Cant", 0, 1,'C', 0);

    $pdf->SetXY(1.1+4.7,$Y);
    $pdf->Cell(1, 0.30, "Unidad", 0, 1,'L', 0);

    $pdf->SetXY(1.1+4.7+2.1,$Y);
    $pdf->MultiCell(4.2, 0.35, utf8_decode("Descripción"), 0, 'L', 0);

    $pdf->SetXY(16.65,$Y);
    $pdf->Cell(1.5, 0.35, utf8_decode("P/U"), 0, 1,'R', 0);

    $pdf->SetXY(16.65+1.8,$Y);
    $pdf->Cell(1.8, 0.35, "Importe", 0, 1,'R', 0);    
}



function DatosInf($pdf, $filename, $Y, $selloCFD, $selloSAT, $CadOri){
    
    $pdf->SetTextColor(0,0,0);
    $pdf->SetFont('arial','B',9); 
    $pdf->SetXY(1.2,22.9+$Y-0.25 -1.5);
    $pdf->Cell(1.7,0.30, "Sello digital del CFDI:", 0, 1,'L', 0);    

        $pdf->SetTextColor(17,71,121);
        $pdf->SetFont('arial','',7); 
        $pdf->SetXY(1.2,+22.9+0.35+$Y-0.2 -1.5);
        $pdf->MultiCell(19.4, 0.25, $selloCFD, 0, 'L', 0);
    
    $pdf->SetTextColor(0,0,0);
    $pdf->SetFont('arial','B',9); 
    $pdf->SetXY(4.2,21.9+2+$Y-1.2-0.2);
    $pdf->Cell(1.7, 0.30, "Sello del SAT:", 0, 1,'L', 0);    

        $pdf->SetTextColor(17,71,121);
        $pdf->SetFont('arial','',7); 
        $pdf->SetXY(4.2,21.9+0.35+2+$Y-1.2-0.2);
        $pdf->MultiCell(16.1, 0.25, $selloSAT, 0, 'L', 0);
        
    $pdf->SetTextColor(0,0,0);    
    $pdf->SetFont('arial','B',9); 
    $pdf->SetXY(4.2,25+$Y-0.7-0.25);
    $pdf->Cell(1.7, 0.30, utf8_decode("Cadena original del complemento de certificación digital del SAT:"), 0, 1,'L', 0);    
    
        $pdf->SetTextColor(17,71,121);
        $pdf->SetFont('arial','',7); 
        $pdf->SetXY(4.2,25.1+0.25+$Y-0.7-0.2);
        $pdf->MultiCell(16.1, 0.25, $CadOri, 0, 'L', 0);
        
    $pdf->SetTextColor(0,0,0);
    $pdf->SetFont('arial','B',10); 
    $pdf->SetXY(4.2,26.36+$Y -0.65);
    $pdf->Cell(15.6, 0.30, utf8_decode("===== Este documento es una representación impresa de un CFDI ====="), 0, 1,'C', 0);    
        
    $pdf->Image($filename,1.2,23.8+$Y-1.1-0.25,3,3,'PNG');
     
}


function ProcesImpTot($ImpTot){
    $ImpTot = number_format($ImpTot, 4); // <== Se agregó el 30 de abril de 2017.
    $ArrayImpTot = explode(".", $ImpTot);
    $NumEnt = $ArrayImpTot[0];
    $NumDec = ProcesDecFac($ArrayImpTot[1]);
    return $NumEnt.".".$NumDec;
}


function ProcesDecFac($Num){
    $FolDec = "";
    if ($Num < 10){$FolDec = "00000".$Num;}
    if ($Num > 9 and $Num < 100){$FolDec = $Num."0000";}
    if ($Num > 99 and $Num < 1000){$FolDec = $Num."000";}
    if ($Num > 999 and $Num < 10000){$FolDec = $Num."00";}
    if ($Num > 9999 and $Num < 100000){$FolDec = $Num."0";}
    return $FolDec;
}    


        
function VerifStatusCFDI($pdf, $StatusCFDI){

    if ($StatusCFDI=="CANCELADO"){

        $pdf->SetLineWidth(0.1);
        $pdf->SetDrawColor(200,0,0);        
        $pdf->SetTextColor(200,0,0);
        $pdf->SetFont('verdana','',53); 
        
        $pdf->RoundedRect(4.4, 7.4-2.5, 12.6, 2.05, 0.4, '');
        $pdf->SetXY(1,8.4-2.5);
        $pdf->Cell(19.4, 0.30, "CANCELADO", 0, 1,'C', 0);    

        $pdf->SetLineWidth(0.02);
        $pdf->SetDrawColor(0,0,0);        
        $pdf->SetTextColor(0,0,0);
    }
}       


